import json
import time

import pytest

from tests.factories import StaticResponseFactory


def test_static_response_from_request(client):
    s_response = StaticResponseFactory(
        route="/api/v5/letters/create",
        response=json.dumps({
            "status": "ok"
        }),
        method="POST",
        status_code=202,
        delay=0
    )
    client.post('/lumine/responses', json=s_response)
    response = client.post(
        '/api/v5/letters/create',
        json={'data': 'exist'}
    )
    assert response.status_code == 202
    assert response.json() == {
        "status": "ok"
    }


@pytest.mark.websockets
def test_request_broadcast(client):
    with client.websocket_connect('/ws') as websocket:
        time.sleep(0.1)  # we need to wait a bit to make sure its running correctly
        client.get('/api/v1/posts')
        data = websocket.receive_json()
        assert data['url'] == '/api/v1/posts'
        assert data['method'] == 'GET'


@pytest.mark.websockets
def test_request_broadcast_custom_method(client):
    with client.websocket_connect('/ws') as websocket:
        time.sleep(0.1)  # we need to wait a bit to make sure its running correctly
        client.post(
            '/api/v1/posts',
            headers={'L-Method': 'REDIS-PUBLISH'},
            json={'data': 'exist'}
        )
        data = websocket.receive_json()
        assert data['url'] == '/api/v1/posts'
        assert data['method'] == 'REDIS-PUBLISH'
        assert data['body'] == {'data': 'exist'}
