import asyncio
import json

from orm.exceptions import NoMatch
from starlette.endpoints import HTTPEndpoint
from starlette.responses import JSONResponse

import lumine.settings as settings
from lumine.models import StaticResponse


class ResponseEndpoint(HTTPEndpoint):
    async def get(self, request):
        response_id = request.path_params['response_id']
        try:
            response = await StaticResponse.objects.get(id=response_id)
        except NoMatch:
            return JSONResponse(status_code=404)
        return JSONResponse(
            {
                'id': response.id,
                'route': response.route,
                'method': response.method,
                'response': response.response,
                'status_code': response.status_code,
                'delay': response.delay,
                'enabled': response.enabled,
            }
        )

    async def put(self, request):
        response_id = request.path_params['response_id']
        updated_response = await request.json()
        try:
            response = await StaticResponse.objects.get(id=response_id)
        except NoMatch:
            return JSONResponse(status_code=404)
        await response.update(**updated_response)
        return JSONResponse(
            {
                'id': response.id,
                'route': response.route,
                'method': response.method,
                'response': response.response,
                'status_code': response.status_code,
                'delay': response.delay,
                'enabled': response.enabled,
            }
        )

    async def delete(self, request):
        response_id = request.path_params['response_id']
        try:
            response = await StaticResponse.objects.get(id=response_id)
        except NoMatch:
            return JSONResponse(status_code=404)
        await response.delete()
        return JSONResponse(status_code=204)


class ResponseToggleEndpoint(HTTPEndpoint):
    async def post(self, request):
        response_id = request.path_params['response_id']

        try:
            response = await StaticResponse.objects.get(id=response_id)
        except NoMatch:
            return JSONResponse(status_code=404)
        else:
            await response.update(enabled=True)
            return JSONResponse(status_code=204)

    async def delete(self, request):
        response_id = request.path_params['response_id']
        try:
            response = await StaticResponse.objects.get(id=response_id)
        except NoMatch:
            return JSONResponse(status_code=404)
        else:
            await response.update(enabled=False)
            return JSONResponse(status_code=204)


class ResponsesEndpoint(HTTPEndpoint):
    async def get(self, request):
        responses = await StaticResponse.objects.all()
        return JSONResponse(
            [
                {
                    'id': response.id,
                    'route': response.route,
                    'method': response.method,
                    'response': response.response,
                    'status_code': response.status_code,
                    'delay': response.delay,
                    'enabled': response.enabled,
                }
                for response in responses
            ]
        )

    async def post(self, request):
        data = await request.json()
        response = await StaticResponse.objects.create(**data)
        return JSONResponse(
            {
                'route': response.route,
                'method': response.method,
                'response': response.response,
                'status_code': response.status_code,
                'delay': response.delay,
                'enabled': response.enabled,
            }
        )


async def get_response(method, route):
    try:
        response = await StaticResponse.objects.get(method=method, route=route, enabled=True)
    except NoMatch:
        return settings.DEFAULT_RESPONSE, settings.DEFAULT_STATUS_CODE

    if response.delay > 0:
        await asyncio.sleep(response.delay)

    try:
        return json.loads(response.response), response.status_code
    except TypeError:
        return response.response, response.status_code
