import json

from tests.factories import StaticResponseFactory


def test_create_static_response(client):
    s_response = StaticResponseFactory(
        route="/api/v5/letters/create",
        response=json.dumps({
            "status": "ok"
        }),
        method="POST"
    )
    response = client.post(
        '/lumine/responses',
        json=s_response
    )
    assert response.status_code == 200
    assert response.json() == s_response


def test_list_static_responses(client):
    s_responses = [StaticResponseFactory() for _ in range(5)]
    for pos_id, s_response in enumerate(s_responses):
        client.post('/lumine/responses', json=s_response)
        # Inject Ids
        s_response['id'] = pos_id + 1

    response = client.get('/lumine/responses')
    assert response.status_code == 200
    assert response.json() == s_responses


def test_list_no_static_responses(client):
    response = client.get('/lumine/responses')
    assert response.status_code == 200
    assert response.json() == []


def test_read_static_response_404(client):
    response = client.get('/lumine/response/1')
    assert response.status_code == 404


def test_read_static_response(client):
    s_response = StaticResponseFactory()
    client.post('/lumine/responses', json=s_response)
    s_response['id'] = 1
    response = client.get('/lumine/response/1')
    assert response.status_code == 200
    assert response.json() == s_response


def test_delete_static_response_404(client):
    response = client.delete('/lumine/response/1')
    assert response.status_code == 404


def test_delete_static_response(client):
    s_response = StaticResponseFactory()
    client.post('/lumine/responses', json=s_response)
    s_response['id'] = 1
    response = client.delete('/lumine/response/1')
    assert response.status_code == 204
    response = client.get('/lumine/response/1')
    assert response.status_code == 404


def test_update_static_response(client):
    s_response = StaticResponseFactory()
    client.post('/lumine/responses', json=s_response)
    s_response_updated = StaticResponseFactory()
    response = client.put('/lumine/response/1', json=s_response_updated)
    s_response_updated['id'] = 1
    assert response.status_code == 200
    assert response.json() == s_response_updated
    response = client.get('/lumine/response/1')
    assert response.status_code == 200
    assert response.json() == s_response_updated


def test_enable_static_response(client):
    s_response = StaticResponseFactory(enabled=False)
    client.post('/lumine/responses', json=s_response)
    response = client.post('/lumine/response/1/toggle')
    assert response.status_code == 204
    s_response['enabled'] = True
    s_response['id'] = 1
    response = client.get('/lumine/response/1')
    assert response.status_code == 200
    assert response.json() == s_response


def test_disable_static_response(client):
    s_response = StaticResponseFactory()
    client.post('/lumine/responses', json=s_response)
    response = client.delete('/lumine/response/1/toggle')
    assert response.status_code == 204
    s_response['enabled'] = False
    s_response['id'] = 1
    response = client.get('/lumine/response/1')
    assert response.status_code == 200
    assert response.json() == s_response
