## Lumine

Starlette backend of the LPF Stack. Lumine is responsible for handling the incoming requests,
and responding to them while also broadcasting them.

### Development

First of all you need to install the requirements as per usual with the following command:
```shell script
pip install -r /app/requirements.txt
```

The you simply execute the following command 
```shell script
python -m lumine.app
```

### Production Deployment

Rename the example dotenv configuration 
```shell script
mv env.example .env
```

Deploy the application using the docker image from gitlab registry 
```shell script
sudo docker run -d -p 9040:8000 --env-file .env --restart always --name lumine registry.gitlab.com/yagdo/luminescence:stable
```

If you want to build and run the image yourself you can use the following command on the cloned directory
```shell script
sudo docker build -t lumine:latest .
```
Deploy the application using your locally build image 
```shell script
sudo docker run -d -p 9040:8000 --env-file .env --restart always --name lumine lumine:latest
```