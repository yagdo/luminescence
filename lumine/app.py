import datetime
import json
import uuid

import uvicorn
from nejma.ext.starlette import WebSocketEndpoint, channel_layer
from starlette.applications import Starlette
from starlette.middleware.cors import ALL_METHODS, CORSMiddleware
from starlette.responses import JSONResponse
from starlette.routing import Route

import lumine.settings as settings
from lumine.requests import EntriesEndpoint
from lumine.responses import (
    ResponseEndpoint, ResponsesEndpoint, ResponseToggleEndpoint, get_response,
)

application = Starlette(
    debug=settings.DEBUG,
    routes=[
        Route(
            '/lumine/response/{response_id:int}',
            endpoint=ResponseEndpoint,
            methods=['GET', 'PUT', 'DELETE', 'OPTIONS']
        ),
        Route(
            '/lumine/response/{response_id:int}/toggle',
            endpoint=ResponseToggleEndpoint,
            methods=['POST', 'DELETE', 'OPTIONS']
        ),
        Route(
            '/lumine/responses',
            endpoint=ResponsesEndpoint,
            methods=['GET', 'POST', 'OPTIONS']
        ),
        Route(
            '/lumine/entries',
            endpoint=EntriesEndpoint,
            methods=['GET', 'DELETE', 'OPTIONS']
        ),
    ],
    on_startup=settings.database.connect,
    on_shutdown=settings.database.disconnect,

)
application.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_headers=['*'],
    allow_methods=ALL_METHODS,
)


@application.websocket_route("/ws")
class Notifications(WebSocketEndpoint):
    encoding = "json"

    async def on_connect(self, websocket, **kwargs):
        await super().on_connect(websocket, **kwargs)
        # adds everyone on a global notifications group
        self.channel_layer.add('notifications_group', self.channel)


@application.exception_handler(404)
async def insert_http_entry(request, exc):
    """
    Return an HTTP 404 page.
    """
    data = await request.body()  # TODO consider using .json() and if fails use body()
    data = data.decode('utf-8')
    try:
        data = json.loads(data)
    except (TypeError, json.decoder.JSONDecodeError):
        pass

    payload = {
        'id': str(uuid.uuid4()),
        'url': request.url.path,
        'method': request.headers.get('L-Method', request.method),
        'headers': dict(request.headers.items()),
        'params': str(request.query_params),
        'body': data,
        'created_at': datetime.datetime.utcnow().isoformat()
    }
    await channel_layer.group_send('notifications_group', payload)
    response, status_code = await get_response(payload['method'], payload['url'])
    return JSONResponse(response, status_code=status_code)


if __name__ == "__main__":
    uvicorn.run(application, host='0.0.0.0', port=8000)
