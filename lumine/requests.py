import json

from starlette.endpoints import HTTPEndpoint
from starlette.responses import JSONResponse

from lumine.models import HTTPEntry


# TODO this will be rewritten
class EntriesEndpoint(HTTPEndpoint):
    async def get(self, request):
        entries = await HTTPEntry.objects.all()
        entries.sort(key=lambda entry: entry.created_at, reverse=True)  # TODO not really efficient

        return JSONResponse(
            [
                {
                    'id': entry.id,
                    'url': entry.url,
                    'method': entry.method,
                    'headers': json.loads(entry.headers),  # TODO convert it to json
                    'params': entry.params,
                    'body': json.loads(entry.body) if entry.body else None,
                    'created_at': entry.created_at.isoformat()
                }
                for entry in entries
            ]
        )

    async def delete(self, request):
        entries = await HTTPEntry.objects.all()
        for entry in entries:
            await entry.delete()
        return JSONResponse(status_code=204)
