import datetime

import orm  # TODO it doesnt seem like its maintained actively, probably will move away from it
import sqlalchemy

from lumine.settings import database

metadata = sqlalchemy.MetaData()


class StaticResponse(orm.Model):
    __tablename__ = 'static_responses'
    __metadata__ = metadata
    __database__ = database

    id = orm.Integer(primary_key=True)
    route = orm.String(max_length=250)
    response = orm.Text()
    method = orm.String(max_length=10)
    status_code = orm.Integer(default=200)
    delay = orm.Integer(default=0)
    enabled = orm.Boolean(default=True)

# TODO enable when ready
# class ForwardResponse(orm.Model):
#     __tablename__ = 'forward_responses'
#     __metadata__ = metadata
#     __database__ = database
#
#     id = orm.Integer(primary_key=True)
#     route = orm.String(max_length=250)
#     method = orm.String(max_length=10)
#     target = orm.String(allow_null=False)
#     enabled = orm.Boolean(default=True)


class HTTPEntry(orm.Model):
    __tablename__ = 'http_entries'
    __metadata__ = metadata
    __database__ = database

    id = orm.Integer(primary_key=True)
    url = orm.String(max_length=250)
    method = orm.String(max_length=10)
    headers = orm.Text(allow_blank=True)
    params = orm.Text(allow_blank=True)
    body = orm.Text(allow_blank=True)
    created_at = orm.DateTime(default=datetime.datetime.utcnow)
