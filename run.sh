#!/bin/sh

alembic upgrade head
gunicorn lumine.app:application -w $WORKERS -k uvicorn.workers.UvicornWorker -b 0.0.0.0:8000
