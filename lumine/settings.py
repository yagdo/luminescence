import json

import databases
from starlette.config import Config

config = Config(".env")

SERVICE_NAME = 'Lumine'

LOG_LEVEL = config('LOG_LEVEL', default='DEBUG')
DEBUG = config('DEBUG', default=False, cast=bool)

RAW_DATABASE_URL = config('DATABASE_URL')
DATABASE_URL = databases.DatabaseURL(RAW_DATABASE_URL)
DB_ECHO = config('DB_ECHO', default=False, cast=bool)

DEFAULT_STATUS_CODE = config('DEFAULT_STATUS_CODE', default=200, cast=int)
DEFAULT_RESPONSE = config('DEFAULT_RESPONSE', default='{}', cast=json.loads)

TESTING = config('TESTING', cast=bool, default=False)
if TESTING:
    DATABASE_URL = DATABASE_URL.replace(database='test_' + DATABASE_URL.database)
    database = databases.Database(url=DATABASE_URL, force_rollback=True)
else:
    database = databases.Database(url=DATABASE_URL)
