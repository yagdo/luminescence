FROM python:3.7-alpine

RUN adduser --uid 1000 --disabled-password --gecos "" --home /home/deploy deploy

RUN apk update && apk add build-base gcc && pip install -U pip

RUN pip install --no-cache-dir gunicorn==20.0.4

COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt

WORKDIR /app
COPY . /app

RUN chown -R deploy:deploy /app --verbose

USER deploy

CMD ["./run.sh"]