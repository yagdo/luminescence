import json

from factory import Factory, fuzzy


def model_to_dict(**kwargs):
    return dict(kwargs)


class StaticResponseFactory(Factory):
    class Meta:
        model = model_to_dict

    route = fuzzy.FuzzyText(length=250)
    response = json.dumps({})
    method = fuzzy.FuzzyText(length=10)
    status_code = fuzzy.FuzzyInteger(low=200, high=500)
    delay = fuzzy.FuzzyInteger(low=0, high=10)
    enabled = True
