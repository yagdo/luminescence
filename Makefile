# Greatly inspired by following gist:
# https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db
TAG := $(shell git log -1 --pretty=format:"%h")

APP_NAME = "lumine"

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
# .PHONY: help
.PHONY: clean clean-pyc help
.DEFAULT_GOAL := help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

# Python package Tasks
clean: clean-pyc ## remove Python artifacts

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	find . -name '.pytest_cache' -exec rm -fr {} +

lint: ## check style with flake8
	isort --recursive .
	flake8 .

stable-deploy: ## Deploy a stable build
	sudo docker run -d -p 9040:8000 --env-file .env --restart always --name lumine registry.gitlab.com/yagdo/luminescence:stable

dev-deploy: ## Deploy a development build
	sudo docker build -t lumine:latest .
	sudo docker run -d -p 9040:8000 --env-file .env --restart always --name lumine lumine:latest
